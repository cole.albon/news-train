import { createContext } from 'react';
import createPersistedState from 'use-persisted-state';
const useCorsProxiesState = createPersistedState('corsProxies');
const defaultCorsProxies = () => {
  return {
    '/.netlify/functions/node-fetch?url=': { checked: true },
    'http://localhost:8080/': { checked: false },
  };
};

export type CorsProxy = {
  checked: boolean;
};

export const CorsProxiesContext = createContext(defaultCorsProxies);

export const useCorsProxies = (initialValue = defaultCorsProxies) => {
  const [corsProxies, setCorsProxies] = useCorsProxiesState(initialValue);
  return {
    corsProxies,
    setCorsProxies,
    factoryReset: () => setCorsProxies(defaultCorsProxies),
  };
};
