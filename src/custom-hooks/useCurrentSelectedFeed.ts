import createPersistedState from 'use-persisted-state';

const useCurrentSelectedFeedState = createPersistedState('currentSelectedFeed');
const defaultCurrentSelectedFeed = '';

export const useCurrentSelectedFeed = (
  initialValue = () => defaultCurrentSelectedFeed
) => {
  const [
    currentSelectedFeed,
    setCurrentSelectedFeed,
  ] = useCurrentSelectedFeedState(initialValue);

  return {
    currentSelectedFeed,
    setCurrentSelectedFeed,
  };
};
