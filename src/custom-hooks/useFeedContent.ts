import createPersistedState from 'use-persisted-state';

const useFeedContentState = createPersistedState('feedContent');

export type FeedContent = {
  id: string
  content: string
  link: string
}

export const useFeedContent = (initialValue = () => []) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  const initialState: any[] = [];
  const [feedContent, setFeedContent] = useFeedContentState(initialState);

  return {
    feedContent: feedContent,
    setFeedContent: setFeedContent,
    removeFeedContent: (idx: any) => {
      const newFeedContent : FeedContent[] = JSON.parse(JSON.stringify(feedContent)).filter((FeedContent: any, index: number) => idx !== index);
      setFeedContent(newFeedContent);
    },
    upsertFeedContent: (upsertFeedContent: any[]) => {
      try {
        const newFeedContent = (JSON.parse(JSON.stringify(feedContent))
          .filter((feedContentItem: any) => !upsertFeedContent.some(upsertFeedContent => upsertFeedContent.link === JSON.parse(JSON.stringify(feedContentItem)).link))
        ).concat(upsertFeedContent);
        setFeedContent(newFeedContent);
      } catch(error) {
        console.log(error);
      }
    },
  };
};
