import createPersistedState from 'use-persisted-state';

const useCurrentSelectedCategoryState = createPersistedState(
  'currentSelectedCategory'
);
const defaultCurrentSelectedCategory = '';

export const useCurrentSelectedCategory = (
  initialValue = () => defaultCurrentSelectedCategory
) => {
  const [
    currentSelectedCategory,
    setCurrentSelectedCategory,
  ] = useCurrentSelectedCategoryState(initialValue);

  return {
    currentSelectedCategory,
    setCurrentSelectedCategory,
  };
};
