import createPersistedState from 'use-persisted-state';

const useCategoriesState = createPersistedState('categories');
const defaultCategories = {"science":{"checked":true},"bitcoin":{"checked":true},"local":{"checked":true},"business":{"checked":true},"world":{"checked":true},"politics":{"checked":true},"technology":{"checked":true},"variety":{"checked":true},"us":{"checked":false}}

export const useCategories = (initialValue = () => defaultCategories) => {
  const [categories, setCategories] = useCategoriesState(initialValue);
  return {
    categories,
    setCategories,
    factoryReset: () => setCategories(defaultCategories),
  };
};
