import createPersistedState from 'use-persisted-state';
import { createContext } from 'react';

const useSettingsState = createPersistedState('settings');

export type Settings = {
  hideProcessedPosts: { label: string; checked: boolean };
  disableMachineLearning: { label: string; checked: boolean };
  mlThresholdDocuments: { label: string; checked: boolean; value: number };
  mlThresholdConfidence: { label: string; checked: boolean; value: number };
  showClassifierOverride: { label: string; checked: boolean } | null;
} | null;

const defaultSettings: Settings = {
  hideProcessedPosts: {
    label: 'hide processed posts',
    checked: true,
  },
  disableMachineLearning: {
    label: 'disable machine learning',
    checked: false,
  },
  mlThresholdDocuments: {
    label: 'ignore machine learning under 100 documents',
    checked: true,
    value: 100,
  },
  mlThresholdConfidence: {
    label: 'machine learning confidence threshold: .99',
    checked: true,
    value: 0.99,
  },
  showClassifierOverride: {
    label: 'show classifier override',
    checked: false,
  },
};

export const SettingsContext = createContext(defaultSettings);

export const useSettings = () => {
  const [settings, setSettings] = useSettingsState(defaultSettings);
  return {
    settings: settings,
    setSettings: setSettings,
    factoryReset: () => setSettings(defaultSettings),
  };
};
