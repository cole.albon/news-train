import { createContext } from 'react';
import createPersistedState from 'use-persisted-state';
const useURIsState = createPersistedState('uris');
// export type URI = { categories: string[]; checked: boolean; text: string[] };
const defaultURIs = {
  "test-one": {
    "URI": "https://gaia.blockstack.org/hub/1ecnyUF9ixVYAyx1QX9326KG68mnuwYd7/share_bitcoin_bitcoin-blockstack-broadcast",
    "checked": "true"
  }
}

export const URIsContext = createContext(defaultURIs);

export const useURIs = () => {
  const [uris, setURIs] = useURIsState(() => defaultURIs);
  return {
    uris,
    setURIs,
    factoryReset: () => setURIs(defaultURIs),
  };
};
