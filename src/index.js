import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.tsx';
import axios from 'axios';
import {hash} from 'tweetnacl';
var fromEntries = require('fromentries')

export const removePunctuation = (text) => {
  return text.replace(/[/?–…".,#!$%^&*;:{}=\-_`~()'’‘“”—]/g,"").replace(/\n/g," ").replace(/\s{2,}/g," ").toLowerCase()
}

export const removeTrackingGarbage = (text) => {
  return text.replace(/read full article at rtcom.*$/g,"")
  .replace(/appeared first on.*$/g,"")
  .replace(/\[httpstheinterceptcom.*$/g,"")
  .replace(/\[httpselectrekcowebstories.*$/g,"")
  .replace(/\n/g," ").replace(/\s{2,}/g," ")
  .toLowerCase()
}

export const hashStr = (text) => {
  return removePunctuation(encodeURIComponent(hash(new Buffer(text)).toString()))
}

export const shortUrl = (text) => {
  const theUrl = new URL(text)
  const newPath = removePunctuation(`${theUrl.hostname}${theUrl.pathname}`)
  return newPath
}

export const fetchViaProxy = (url, corsproxies) => {
  return new Promise((resolve, reject) => {
    const [corsproxy, ...rest] = corsproxies;
    [corsproxy]
      .flat()
      .filter(corsproxyitem => {
        return !!corsproxyitem;
      })
      .foreach(corsproxyitem => {
        axios
          .get(`${corsproxyitem}${url}`)
          .then(response => {
            resolve(response);
            return;
          })
          .catch(() => {
            if (rest.length === 0) {
              reject();
            }
            fetchViaProxy(url, rest);
          });
      });
  });
};

export const unique = (arr) => {
    var a = [];
    var l = arr.length;
    for(var i=0; i<l; i++) {
        for(var j=i+1; j<l; j++) {
            // If a[i] is found later in the array
            if (arr[i] === arr[j])
              j = ++i;
        }
        a.push(arr[i]);
    }
    return a;
};

export const fetchMultiViaProxy = (urls, corsProxies) => {
  return new Promise((resolve, reject) => {
    const fetchQueue = [];
    Object.values(urls).map(url =>
      fetchQueue.push(
        new Promise((resolve, reject) => {
          fetchViaProxy(url, corsProxies)
            .then((fetchedContent) => {
              return [fetchedContent].flat().forEach(fetchedContentItem => {
                resolve([url, fetchedContentItem]);
              });
            })
            .catch(() => resolve({}));
        })
      )
    );
    Promise.all(fetchQueue).then((fetchedContent) => {
      resolve(fromEntries(fetchedContent));
    });
  });
};
ReactDOM.render(<App />, document.getElementById('App'));
