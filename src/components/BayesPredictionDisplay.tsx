import React, {
  useContext,
} from 'react';
import LearnButton from './LearnButton';
import { BayesPredictionContext } from './ApplyBayesPrediction'
import { CategoryContext } from './Categories';
import {useSettings} from '../custom-hooks/useSettings'
import {
  Box,
  Typography
} from '@material-ui/core'
import {
  ThumbDown,
  ThumbUp
} from '@material-ui/icons'

const BayesPredictionDisplay = () => {
  const categoryContext = useContext(CategoryContext);
  const category = categoryContext.toString();

  const bayesPredictionContext = useContext(BayesPredictionContext)
  const bayesPrediction = Object.assign(bayesPredictionContext)

  const {settings} = useSettings()
  const {disableMachineLearning} = JSON.parse(JSON.stringify(settings))

  if (disableMachineLearning.checked) {
    return <></>
  }

  return (
    <Box style={{display: 'flex', flexDirection: 'row', justifyContent: 'center',fontSize:'x-large', marginTop:25, marginBottom:50, borderBottomStyle:'solid'}}>
        <Typography  variant="h4" key={'notgoodPrediction'}>{parseFloat(bayesPrediction.likelihoods ?.filter((likelihood: {proba: string}) => likelihood.proba !== `NaN`).find(
                (likelihood: {category: string}) => likelihood && likelihood.category === 'notgood'
              )?.proba || 0.0 )?.toFixed(2)
            }
        </Typography>
        <LearnButton
          classification="notgood"
          icon={<ThumbDown style={{ fontSize: 60 }} />}
        />
        {category}
        <LearnButton
          classification="good"
          icon={<ThumbUp style={{ fontSize: 60 }} />}
        />
        <Typography  variant="h4" key={'goodPrediction'}>{parseFloat(bayesPrediction.likelihoods ?.find(
              (likelihood: {category: string}) => likelihood && likelihood.category === 'good'
            )?.proba || 0.0 )?.toFixed(2)
          }
        </Typography>
    </Box>

  );
};

export default BayesPredictionDisplay;
