import React, { useCallback } from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';
import { useSettings } from '../custom-hooks/useSettings';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const AppSettingsToggle = (props: {
  name: string;
}) => {
  const { settings, setSettings } = useSettings();
  const setSettingsCallback = useCallback(() => {
    const newSetting = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(settings)))
            .filter((setting: [string, object]) => setting[0] === props.name)
            .map((setting: [string, object]) => {
              return [
                setting[0],
                {
                  ...fromEntries(
                    entries({
                      ...(setting[1] as Record<string, unknown>),
                    })
                      .filter(
                        (attribute: [string, boolean]) =>
                          attribute[0] === 'checked'
                      )
                      .map((attribute: [string, boolean]) => [attribute[0], !attribute[1]])
                  ),
                  ...fromEntries(
                    entries({
                      ...(setting[1] as Record<string, unknown>),
                    }).filter((attribute: [string, boolean]) => attribute[0] !== 'checked')
                  ),
                },
              ];
            })
        ),
      })
    );
    setSettings({ ...JSON.parse(JSON.stringify(settings)), ...newSetting });
  }, [settings, props.name, setSettings]);

  return (
    <div style={{display: 'flex', flexDirection: 'column'}}>
      {entries(JSON.parse(JSON.stringify(settings)))
        .filter((setting: [string, unknown]) => setting[0] === props.name)
        .map((setting: [string, unknown]) => {
          const attributes = setting[1] as Record<string, unknown>;
          return (
            <FormControlLabel
              key={setting[0]}
              control={
                <Switch
                  checked={Object.values(
                    fromEntries(
                      entries(attributes).filter(
                        (attribute: [string, unknown]) => attribute[0] === 'checked'
                      )
                    )
                  ).some(checked => checked)}
                  onChange={() => setSettingsCallback()}
                  name={props.name}
                />
              }
              label={`${
                [
                  entries(attributes).find(
                    (attribute: [string, unknown]) => attribute[0] === 'label'
                  ),
                ]
                  .flat()
                  .slice(-1)[0]
              }`}
            />
          );
        })}
    </div>
  );
};

export default AppSettingsToggle;
