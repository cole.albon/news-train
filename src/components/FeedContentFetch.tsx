import React, {
  FunctionComponent,
  useCallback,
  createContext,
  useEffect,
  useState,
} from 'react';
import axios from 'axios';
import { useFeeds } from '../custom-hooks/useFeeds';
import { useCategories } from '../custom-hooks/useCategories';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';
var fromEntries = require('fromentries')
var entries = require('object.entries')

export const FetchedContentContext = createContext({});

const fetchFeedContent = (feedUrl: string, corsProxies: string[]): Promise<object> => {
  return new Promise((resolve, reject) => {
    const [corsProxy, ...rest] = corsProxies;
    [corsProxy]
      .flat()
      .filter(corsProxyItem => {
        return !!corsProxyItem;
      })
      .forEach(corsProxyItem => {
        axios
          .get(`${corsProxyItem}${feedUrl}`)
          .then(response => {
            resolve(response);
            return;
          })
          .catch(() => {
            if (rest.length === 0) {
              reject();
            }
            fetchFeedContent(feedUrl, rest);
          });
      });
  });
};

const fetchFeedContentMulti = (feeds: string[], corsProxies: string[]): Promise<object> => {
  return new Promise((resolve, reject) => {
    const feedQueue: object[] = [];
    Object.values(feeds).map(feed =>
      feedQueue.push(
        new Promise((resolve, reject) => {
          fetchFeedContent(feed, corsProxies)
            .then((fetchedContent: object) => {
              return [fetchedContent].flat().forEach(fetchedContentItem => {
                resolve([feed, fetchedContent]);
              });
            })
            .catch(() => resolve({}));
        })
      )
    );
    Promise.all(feedQueue).then((fetchedContent: object) => {
      resolve(fromEntries(fetchedContent));
    })
    .catch(() => resolve({}))
  });
};

const FeedContentFetch: FunctionComponent = ({ children }) => {
  const [fetchedContent, setFetchedContent] = useState({});

  const setFetchedContentCallback = useCallback(
    (content: object) => {
      const newContent = { ...content };
      setFetchedContent(newContent);
    },
    [setFetchedContent]
  );
  const { feeds } = useFeeds();
  const { corsProxies } = useCorsProxies();
  const { categories } = useCategories();

  useEffect(() => {
    const checkedCategories = entries(JSON.parse(JSON.stringify(categories)))
    .filter(
      (categoryEntry: [string, unknown]) =>
        Object.assign(categoryEntry[1] as object).checked === true
    )
    .map((categoryEntry: [string, unknown]) => categoryEntry[0]);

    const checkedCategoryFeeds = entries(JSON.parse(JSON.stringify(feeds)))
    .filter((feedEntry: [string, unknown]) => Object.assign(feedEntry[1] as object).checked === true)
    .filter((feedEntry: [string, unknown]) =>
      checkedCategories.some((category: string) =>
        [Object.assign(feedEntry[1] as object).categories]
          .flat()
          .some((feedCategory: string) => feedCategory === category)
      )
    )
    .map((feedEntry: [string, unknown]) => feedEntry[0]);

    const checkedCorsProxies = entries(JSON.parse(JSON.stringify(corsProxies)))
    .filter(
      (corsProxyEntry: [string, unknown]) =>
        Object.assign(corsProxyEntry[1] as object).checked === true
    )
    .map((corsProxyEntry: [string, unknown]) => corsProxyEntry[0])
    .filter((noblanks: object) => !!noblanks);

    fetchFeedContentMulti(checkedCategoryFeeds, checkedCorsProxies)
      .then(
        (fetchedContent: object) => {
          return setFetchedContentCallback(fetchedContent);
        }
      );
    }, [corsProxies, categories, feeds, setFetchedContentCallback])

  const fetchedContentObj = JSON.parse(JSON.stringify(fetchedContent))

  return (
    <FetchedContentContext.Provider value={fetchedContentObj}>
      <>{children}</>
    </FetchedContentContext.Provider>
  );
};

export default FeedContentFetch;
