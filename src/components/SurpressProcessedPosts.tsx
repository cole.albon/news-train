import React, { FunctionComponent , useContext } from 'react';

import { ProcessedPostsContext } from './ProcessedPostsFetch';
import { PostContext } from './Posts';
import htmlToText from 'html-to-text';
import {useSettings} from '../custom-hooks/useSettings'
import { stringSimilarity } from "string-similarity-js";
import { removePunctuation, removeTrackingGarbage } from '../index.js'

const SurpressProcessedPosts: FunctionComponent = ({ children }) => {
  const {settings} = useSettings()
  const {hideProcessedPosts} = JSON.parse(JSON.stringify(settings))
  const postContext = useContext(PostContext);
  const post = Object.assign(postContext)
  const processedPostsContext = useContext(ProcessedPostsContext)
  const processedPosts = [processedPostsContext].flat().slice()

  if (hideProcessedPosts.checked === false) {
    return (<>{children}</>)
  }

  const theTitle = `${post.title}`
  const theDescription = `${post.description}`
  const descriptionText = htmlToText.fromString(theDescription, {
    ignoreHref: true,
    ignoreImage: true,
  });
  const mlText = removeTrackingGarbage(removePunctuation(`${theTitle} ${descriptionText}`))

  if (mlText === '') {
    return (<></>)
  }

  return (
    !!processedPosts.find(postItem => {
      const similarity = stringSimilarity(`${removeTrackingGarbage(removePunctuation(postItem))}`, mlText)
      return similarity > .85
    }) ? <></> : <>{children}</>
  )
};

export default SurpressProcessedPosts;
