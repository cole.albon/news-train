import React, { FunctionComponent, useContext } from 'react';
import {Button, Typography} from '@material-ui/core';
import { BlockstackSessionContext } from './BlockstackSessionProvider';
import { showConnect } from '@stacks/connect';

const BlockstackSignin: FunctionComponent = () => {
  const blockstackSessionContext = useContext(BlockstackSessionContext);
  const blockstackSession = Object.assign(blockstackSessionContext);

  const authenticate = () => {
    showConnect({
      appDetails: {
        name: 'cafe-society',
        icon: 'https://cafe-society.news/logo192.png',
      },
      redirectTo: '/',
      // onFinish: () => {
      //   blockstackSession.loadUserData();
      // },
      finished: () => {
        window.location.reload();
      },
      userSession: blockstackSession.userSession,
    });
  };

  return (
    <>
      {[blockstackSession.isUserSignedIn()]
        .filter(signedIn => !signedIn)
        .map(() => {
          return (
            <Button key="blockstacksignin" onClick={() => authenticate()}>
              <Typography variant="h3">sign in</Typography>
            </Button>
          );
        })}
    </>
  );
};

export default BlockstackSignin;
