import React, { FunctionComponent, useContext } from 'react';
import { AppSettingsContext } from './AppSettingsProvider';

const AppSettingsJSON: FunctionComponent = () => {
  const appSettingsContext = useContext(AppSettingsContext);
  const appSettings = { ...appSettingsContext };
  return <pre>{JSON.stringify(appSettings, null, 2)}</pre>;
};

export default AppSettingsJSON;
