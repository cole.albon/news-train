import React, { FunctionComponent, Suspense } from 'react';
import PropTypes from 'prop-types';
import useSWR from 'swr';

import { AppConfig, UserSession } from '@stacks/connect';
import { Storage, StorageOptions } from '@stacks/storage';

const appConfig = new AppConfig(['store_write', 'publish_data']);
const userSession: UserSession = new UserSession();
const storageOptions: StorageOptions = { userSession };
const storage = new Storage(storageOptions); // eslint-disable-line @typescript-eslint/no-unused-vars

const fetcher = () => {
  return new Promise(resolve => {
    [appConfig]
      .filter(appConfig => !!appConfig)
      .map(() => {
        return resolve(userSession);
      });
  });
};

export const BlockstackSessionContext = React.createContext({});
export const BlockstackStorageContext = React.createContext({});

const BlockstackSessionProvider: FunctionComponent = ({ children }) => {

  const { data } = useSWR('blockstackSession', fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    revalidateOnFocus: false
  });
  const blockstackSession: UserSession = Object.assign(
    data as Record<string, unknown>
  );

  const blockstackStorage = new Storage(storageOptions);

  return (
    <Suspense fallback={<>blockstack login</>}>
      <BlockstackSessionContext.Provider value={blockstackSession}>
        <Suspense fallback={<>blockstack login</>}>
          <BlockstackStorageContext.Provider value={blockstackStorage}>
            {children}
          </BlockstackStorageContext.Provider>
        </Suspense>
      </BlockstackSessionContext.Provider>
    </Suspense>
  );
};

BlockstackSessionProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
export default BlockstackSessionProvider;
