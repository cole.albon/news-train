import React, { FunctionComponent } from 'react';
import { Box, Divider } from '@material-ui/core';
import CorsProxiesAdd from './CorsProxiesAdd';
import CorsProxiesJSON from './CorsProxiesJSON';
import CorsProxiesReset from './CorsProxiesReset';
import CorsProxyToggle from './CorsProxyToggle';
import CorsProxyDelete from './CorsProxyDelete';
import { RouteComponentProps } from '@reach/router';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';

const CorsProxies: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const { corsProxies } = useCorsProxies();
  // const corsProxiesContext = useContext(CorsProxiesContext);
  // const corsProxies = { ...corsProxiesContext };
  return (
    <>
      <Box p={1}>
        <CorsProxiesAdd />
      </Box>
      <Box p={1} maxWidth={600}>
        {Object.keys(JSON.parse(JSON.stringify(corsProxies))).map(corsProxy => {
          return (
            <Box display="flex" key={corsProxy}>
              <Box width="100%">
                <CorsProxyToggle text={corsProxy} />
              </Box>
              <Box flexShrink={0}>
                <CorsProxyDelete text={corsProxy} />
              </Box>
            </Box>
          );
        })}
      </Box>

      <Box p={1}>
        <CorsProxiesReset />
      </Box>
      <Box display="none">
        <Divider />
        <CorsProxiesJSON />
      </Box>
      <Box display="none">
        <Divider />
        <pre>{JSON.stringify(props, null, 2)}</pre>
      </Box>
    </>
  );
};

export default CorsProxies;
