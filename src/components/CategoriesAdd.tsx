import React, {
  FunctionComponent,
  useState,
  useCallback,
} from 'react';

import { TextField } from '@material-ui/core';
import { useCategories } from '../custom-hooks/useCategories';

const CategoriesAdd: FunctionComponent = () => {
  const [inputValue, setInputValue] = useState('');
  const { categories, setCategories } = useCategories();

  const setInputCallback = useCallback(
    (newInputValue: string) => {
      setInputValue(newInputValue);
    },
    [setInputValue]
  );

  const addCategoryCallback = useCallback(() => {
    const newCategory = JSON.parse(`{"${inputValue}": {"checked": true}}`);
    setCategories({ ...newCategory, ...JSON.parse(JSON.stringify(categories)) });
  }, [categories, setCategories, inputValue]);

  return (
    <>
      <TextField
        id="addCategoryTextField"
        placeholder="add category here"
        value={inputValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .forEach(() => {
              addCategoryCallback();
              setInputCallback('');
            });
        }}
        onChange={event => {
          setInputCallback(event.target.value);
        }}
        fullWidth
      />
    </>
  );
};

export default CategoriesAdd;
