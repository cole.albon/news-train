import React, { FunctionComponent } from 'react';
import { useDispatchers } from '../custom-hooks/useDispatchers';

const DispatchersJSON: FunctionComponent = () => {
  const { dispatchers } = useDispatchers();
  return <pre>{JSON.stringify(dispatchers, null, 2)}</pre>;
};

export default DispatchersJSON;
