import React, {
  FunctionComponent,
  useContext,
} from 'react';
import PropTypes from 'prop-types';
import { BlockstackStorageContext } from './BlockstackSessionProvider';
import { CategoryContext } from './Categories';
import useSWR from 'swr';
import { useDispatchers } from '../custom-hooks/useDispatchers';
import { BlockstackFilenamesContext } from './BlockstackFilenamesFetch';
var entries = require('object.entries')

export const DispatchersContext = React.createContext({});

const DispatchersFetch: FunctionComponent = ({ children }) => {
  const {dispatchers} = useDispatchers()
  const categoryContext = useContext(CategoryContext);
  const category = categoryContext.toString();
  const blockstackStorageContext = useContext(BlockstackStorageContext);
  const blockstackStorage = Object.assign(blockstackStorageContext);
  const blockstackFilenamesContext = useContext(BlockstackFilenamesContext);
  const blockstackFilenames = { ...blockstackFilenamesContext };

  const fetcher = (category: string, blockstackStorage: {getFile: (filename: string, options: object) => Promise<string>}, bustMemoizer?: string) => {
    return new Promise(resolve => {
      const fetchQueue: object[] = []
      Object.values(blockstackFilenames).filter((filename: unknown) => {
        return filename === `dispatchers_${category}`
      }).forEach((filename: unknown) => {
        fetchQueue.push(
          blockstackStorage.getFile(`dispatchers_${category}`, {
            decrypt: true
          })
          .then((fetchedContent: string) => {
            const fetchedDispatchers = JSON.parse(fetchedContent)
            resolve(fetchedDispatchers)
          })
          .catch((error: object) => {
            const dispatchersForCategory = {...[entries(Object.assign(dispatchers as object)).filter((entryItem: [string, unknown]) => {
                  return entryItem[0] === category
                })
                .map((entryItem: [string, object]) => entryItem[1])
              ]
              .flat()
              .find(() => true) as object
            }
            resolve(dispatchersForCategory)
          })
        )
      })
      Promise.all(fetchQueue)
      .then(() => {
        resolve({})
      })
    })
  };


  const { data } = useSWR([category, blockstackStorage, 'dispatchers'], fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    dedupingInterval: 50 * 1000,
    revalidateOnFocus: true
  });

  const fetchedDispatchers: object = Object.assign(
    data as Record<string, unknown>
  );

  return (
    <DispatchersContext.Provider value={fetchedDispatchers}>
      {children}
    </DispatchersContext.Provider>
  );
};

DispatchersFetch.propTypes = {
  children: PropTypes.node.isRequired,
};

export default DispatchersFetch;
