import React, {
  useCallback,
  FunctionComponent,
  Fragment,
} from 'react';
import { IconButton } from '@material-ui/core';
import DeleteOutlined from '@material-ui/icons/DeleteOutlined';
import { useFeeds } from '../custom-hooks/useFeeds';

var fromEntries = require('fromentries')
var entries = require('object.entries')

const FeedDelete: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { feeds, setFeeds } = useFeeds();
  const deleteFeed = useCallback(() => {
    const newFeeds = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(feeds))).filter((feedEntry: [string, object]) => feedEntry[0] !== props.text)
        ),
      })
    );
    setFeeds(newFeeds);
  }, [feeds, props.text, setFeeds]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(feeds)))
        .filter((feedEntry: [string, object]) => feedEntry[0] === props.text)
        .map((feedEntry: [string, object]) => {
          return (
            <Fragment key={`${feedEntry[0]}`}>
              <IconButton aria-label="Delete Feed" onClick={deleteFeed}>
                <DeleteOutlined />
              </IconButton>
            </Fragment>
          );
        })}
    </Fragment>
  );
};

export default FeedDelete;
