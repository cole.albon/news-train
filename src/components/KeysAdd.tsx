import React, {
  FunctionComponent,
  useState,
  useCallback,
} from 'react';
import { TextField } from '@material-ui/core';
import { useKeys } from '../custom-hooks/useKeys';
var chloride = require('chloride')
var fromEntries = require('fromentries')
var entries = require('object.entries')

const KeysAdd: FunctionComponent = () => {
  const [labelValue, setLabelValue] = useState('');
  const [publicKeyValue, setPublicKeyValue] = useState('');
  const [secretKeyValue, setSecretKeyValue] = useState('');
  const { keys, setKeys } = useKeys();

  const setLabelValueCallback = useCallback(
    (newLabelValue: string) => {
      setLabelValue(newLabelValue);
    },
    [setLabelValue]
  );
  const setPublicKeyCallback = useCallback(
    (newPublicKey: string) => {
      console.log(newPublicKey)
      setPublicKeyValue(newPublicKey);
    },
    [setPublicKeyValue]
  );
  const setSecretKeyCallback = useCallback(

    (newSecretKey: string) => {
      console.log(newSecretKey)
      setSecretKeyValue(newSecretKey);
    },
    [setSecretKeyValue]
  );

  const addKeyCallback = useCallback(() => {
    return [
      ...[`${labelValue}`]
      .filter(() => `${labelValue}` !== '')
      .filter(() => `${publicKeyValue}` === '')
      .filter(() => `${secretKeyValue}` === '')
      .map(labelValue => {
        var rawKeypair = chloride.crypto_box_keypair()
        console.log(rawKeypair)
        const pubkeyStr = Buffer.from(rawKeypair.publicKey).toString('base64')
        const secretKeyStr = Buffer.from(rawKeypair.secretKey).toString('base64')
        const keypairWithLabel = JSON.parse(`{"${labelValue}": {
          "publicKey": "${pubkeyStr}",
          "secretKey": "${secretKeyStr}",
          "checked": "true"
        }}`)
        console.log(keypairWithLabel)
        // const newKey = JSON.parse(`{"${labelValue}": {"checked": true}}`);
        setKeys({ ...keypairWithLabel, ...JSON.parse(JSON.stringify(keys)) });
        setLabelValue('');
        return null
      }),
      ...[`${labelValue}`]
      .filter(() => `${labelValue}` !== '')
      .filter(() => `${publicKeyValue}${secretKeyValue}` !== '')
      .map(labelValue => {
        const keypair = JSON.parse(`{
          "publicKey": "${publicKeyValue}",
          "secretKey": "${secretKeyValue}",
          "checked": "true"
        }`)

        const keypairWithLabel = JSON.parse(`{"${labelValue}": ${JSON.stringify(fromEntries(entries(keypair).filter((entryItem: [string, unknown]) => entryItem[1] !== '' )))}}`)
        console.log({ ...keypairWithLabel, ...JSON.parse(JSON.stringify(keys)) })
        setKeys({ ...keypairWithLabel, ...JSON.parse(JSON.stringify(keys)) });
        setLabelValue('');
        return null
      })
    ].find(() => true)
  }, [keys, setKeys, labelValue, publicKeyValue, secretKeyValue]);

  const onSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    console.log(event)
  }

  return (
    <form onSubmit={onSubmit}>
      <TextField
        id="addKeyLabel"
        placeholder="key label"
        value={labelValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .forEach(() => {
              addKeyCallback()
            });
        }}
        onChange={event => {
          setLabelValueCallback(event.target.value);
        }}
        fullWidth
      />
      <TextField
        id="publicKeyValue"
        placeholder="public key curve25519_pk"
        value={publicKeyValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .forEach(() => {
              addKeyCallback();
              setPublicKeyCallback('');
            });
        }}
        onChange={event => {
          setPublicKeyCallback(event.target.value);
        }}
        fullWidth
      />
      <TextField
        id="secretKeyValue"
        placeholder="secret key curve25519_sk"
        value={secretKeyValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .forEach(() => {
              addKeyCallback();
            });
        }}
        onChange={event => {
          setSecretKeyCallback(event.target.value);
        }}
        fullWidth
      />

      <br />
      <pre>
        {JSON.stringify(keys, null, 2)}
      </pre>
    </form>
  );
};

export default KeysAdd;
