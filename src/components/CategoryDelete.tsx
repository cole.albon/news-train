import React, {
  // useContext,
  useCallback,
  FunctionComponent,
  Fragment,
} from 'react';
import { IconButton } from '@material-ui/core';
import DeleteOutlined from '@material-ui/icons/DeleteOutlined';
import { useCategories } from '../custom-hooks/useCategories';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const CategoryDelete: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { categories, setCategories } = useCategories();

  const deleteCategory = useCallback(() => {
    const newCategories = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(categories))).filter(
            (category: [string, unknown]) => category[0] !== props.text
          )
        ),
      })
    );
    setCategories(newCategories);
  }, [categories, props.text, setCategories]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(categories)))
        .filter((category: [string, unknown]) => category[0] === props.text)
        .map((category: [string, unknown]) => {
          return (
            <Fragment key={`${category}`}>
              <IconButton aria-label="Delete Category" onClick={deleteCategory}>
                <DeleteOutlined />
              </IconButton>
            </Fragment>
          );
        })}
    </Fragment>
  );
};

export default CategoryDelete;
