import React, { FunctionComponent } from 'react';
import { Box } from '@material-ui/core';
import BlockstackSignIn from './BlockstackSignIn';
import BlockstackSignOut from './BlockstackSignOut';

const BlockstackSettings: FunctionComponent = () => {
  return (
      <Box p={1}>
        <BlockstackSignIn />
        <BlockstackSignOut />
      </Box>
  );
};

export default BlockstackSettings;
