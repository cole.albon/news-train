import React, { FunctionComponent } from 'react';
import Button from '@material-ui/core/Button';
import { useCategories } from '../custom-hooks/useCategories';

const CategoriesReset: FunctionComponent = () => {
  const { factoryReset } = useCategories();
  return (
    <Button key="categoriesreset" onClick={() => factoryReset()}>
      reset categories
    </Button>
  );
};
export default CategoriesReset;
