import React, { FunctionComponent } from 'react';
import Button from '@material-ui/core/Button';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';

const CorsProxiesReset: FunctionComponent = () => {
  const { factoryReset } = useCorsProxies();
  return (
    <Button key="corsproxiesreset" onClick={() => factoryReset()}>
      reset cors proxies
    </Button>
  );
};
export default CorsProxiesReset;
