import React, { FunctionComponent, useContext } from 'react';
import {Button, Typography} from '@material-ui/core';
import { BlockstackSessionContext } from './BlockstackSessionProvider';

const BlockstackSignOut: FunctionComponent = () => {
  const blockstackSessionContext = useContext(BlockstackSessionContext);
  const blockstackSession = Object.assign(blockstackSessionContext);

  return (
    <>
      {[blockstackSession.isUserSignedIn()]
        .filter(signedIn => !!signedIn)
        .map(() => {
          return (
            <Button
              key="blockstacksignout"
              onClick={() =>
                blockstackSession.signUserOut(window.location.origin)
              }
            >
              <Typography variant="h2">sign out</Typography>
            </Button>
          );
        })}
    </>
  );
};

export default BlockstackSignOut;
