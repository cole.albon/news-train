import React, { FunctionComponent , useContext } from 'react';
import { BayesPredictionContext } from './ApplyBayesPrediction';
import {useSettings} from '../custom-hooks/useSettings'

const SurpressNotGoodAboveThresholdPosts: FunctionComponent = ({ children }) => {
  const bayesPredictionContext = useContext(BayesPredictionContext)
  const {settings} = useSettings()
  const {mlThresholdConfidence, disableMachineLearning} = JSON.parse(JSON.stringify(settings))
  const bayesPrediction = Object.assign(bayesPredictionContext)
  if (disableMachineLearning.checked) {
    return <>{children}</>
  }
  const surpress = parseFloat(bayesPrediction.likelihoods ?.filter((likelihood: {proba: string}) => likelihood.proba !== `NaN`).find(
            (likelihood: any) => likelihood && likelihood.category === 'notgood'
          )?.proba || 0.0) > mlThresholdConfidence.value

  if (!!surpress) {
    return (<></>)
  }
  return (<>{children}</>)
};

export default SurpressNotGoodAboveThresholdPosts;
