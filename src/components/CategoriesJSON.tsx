import React, { FunctionComponent } from 'react';
import { useCategories } from '../custom-hooks/useCategories';

const CategoriesJSON: FunctionComponent = () => {
  const { categories } = useCategories();
  return <pre>{JSON.stringify(categories, null, 2)}</pre>;
};

export default CategoriesJSON;
