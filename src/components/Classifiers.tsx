import React, { FunctionComponent, Suspense} from 'react';
import {
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Link,
  Box
} from '@material-ui/core';
import { CssBaseline, Container } from '@mui/material';
import { RouteComponentProps } from  '@reach/router';
import NavBar from './NavBar';
import { useCategories } from '../custom-hooks/useCategories';
import ClassifierFetch from './ClassifierFetch'
import ClassifierEdit from './ClassifierEdit'
import DispatchersFetch from './DispatchersFetch'
import BlockstackFilenamesFetch from './BlockstackFilenamesFetch'
import Dispatchers from './Dispatchers'

import { CategoryContext } from './Categories'

const Classifiers: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const { categories } = useCategories()
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const handleAccordionChange = (panel: string) => (
    event: React.ChangeEvent<{}>,
    isExpanded: boolean
  ) => {
    setExpanded(isExpanded ? panel : false);
  };

  const checkedCategories = Object.entries(JSON.parse(JSON.stringify(categories)))
    .filter(category => Object.assign(category[1] as object).checked === true)
    .map(category => category[0]);

  return (
    <Suspense fallback={<>fetching blockstack filenames</>}>
      <BlockstackFilenamesFetch>
     <CssBaseline />
     <Container maxWidth="sm">
      <Box sx={{display: 'flex', flexDirection: 'column'}}>
      <NavBar>
        <Link href="/news">news</Link>
        <Link href="/about">about</Link>
        <Link href="/settings">settings</Link>
      </NavBar>
      <Divider />

          {
           Object.values(checkedCategories).map((category) => {
          return (
            <Accordion
              key={category}
              expanded={expanded === category}
              onChange={handleAccordionChange(category)}
              TransitionProps={{ unmountOnExit: true }}
            >
              <AccordionSummary>{category.toString()}</AccordionSummary>
                <AccordionDetails>
                  <CategoryContext.Provider  key={category.toString()} value={category.toString()}>
                    <br />
                    <Suspense fallback={<>{`fetching ${category.toString()} classifiers`}</>} >
                        <ClassifierFetch>
                          <ClassifierEdit />
                        </ClassifierFetch>
                    </Suspense>
                    <br />
                    <Suspense fallback={<>{`fetching ${category.toString()} classifier dispatchers`}</>}>
                      <DispatchersFetch>
                        <Dispatchers />
                      </DispatchersFetch>
                    </Suspense>
                  </CategoryContext.Provider>
                </AccordionDetails>
            </Accordion>
          )
          })
        }

    </ Box>
    </ Container>
    </ BlockstackFilenamesFetch>
  </ Suspense>
  )
}

export default Classifiers;
