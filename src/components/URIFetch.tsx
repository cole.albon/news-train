import React, {
  FunctionComponent,
  useContext,
} from 'react';
import PropTypes from 'prop-types';
import { BlockstackStorageContext } from './BlockstackSessionProvider';
import { CategoryContext } from './Categories';
import useSWR from 'swr';

export const URIContext = React.createContext({});

const URIFetch: FunctionComponent = ({ children }) => {
  const categoryContext = useContext(CategoryContext);
  const category = categoryContext.toString();
  const blockstackStorageContext = useContext(BlockstackStorageContext);
  const blockstackStorage = Object.assign(blockstackStorageContext);

  const fetcher = (category: string, blockstackStorage: any) => {
    return new Promise(resolve => {
      blockstackStorage.getFile(`uri_${category}`, {
        decrypt: true,
      })
      .then((fetchedContent: string) => {
        var newURI = JSON.parse(fetchedContent)
        resolve(newURI)
      })
      .catch((error: object) => {
        console.log(error)
        resolve({})
      })
    });
  };

  const { data } = useSWR([category, blockstackStorage], fetcher, {
    suspense: true,
    shouldRetryOnError: false,
    dedupingInterval: 60 * 1000,
    revalidateOnFocus: false
  });

  const uri: object = Object.assign(
    data as Record<string, unknown>
  );

  return (
    <URIContext.Provider value={uri}>
      {children}
    </URIContext.Provider>
  );
};

URIFetch.propTypes = {
  children: PropTypes.node.isRequired,
};

export default URIFetch;
