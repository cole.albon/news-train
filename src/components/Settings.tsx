import React, { FunctionComponent, Fragment } from 'react';
import {
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Link,
  Box,
  Typography
} from '@material-ui/core';
import { CssBaseline, Container } from '@mui/material';

import { RouteComponentProps } from '@reach/router';
import AppSettings from './AppSettings';
import CorsProxies from './CorsProxies';
import BlockstackSettings from './BlockstackSettings';
import Categories from './Categories';
import Feeds from './Feeds';
import Keys from './Keys';
import NavBar from './NavBar';

const Settings: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const [expanded, setExpanded] = React.useState<string | false>(false);
  const handleAccordionChange = (panel: string) => (
    event: React.ChangeEvent<{}>,
    isExpanded: boolean
  ) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <Fragment>
     <CssBaseline />
     <Container maxWidth="sm">
      <Box sx={{display: 'flex', flexDirection: 'column'}}>
        <NavBar>
          <Link href="/news">news</Link>
          <Link href="/about">about</Link>
          <Link href="/classifiers">classifiers</Link>
        </NavBar>
        <Divider />
        <Accordion
          expanded={expanded === 'categories'}
          onChange={handleAccordionChange('categories')}
        >
          <AccordionSummary><Typography variant="h1">categories</Typography></AccordionSummary>
          <AccordionDetails>
            <Categories />
          </AccordionDetails>
        </Accordion>

        <Accordion
          expanded={expanded === 'feeds'}
          onChange={handleAccordionChange('feeds')}
        >
          <AccordionSummary><Typography variant="h1">feeds</Typography></AccordionSummary>
          <AccordionDetails>
            <Feeds />
          </AccordionDetails>
        </Accordion>

        <Accordion
          expanded={expanded === 'app'}
          onChange={handleAccordionChange('app')}
        >
          <AccordionSummary><Typography variant="h1">settings</Typography></AccordionSummary>
          <AccordionDetails>
            <AppSettings />
          </AccordionDetails>
        </Accordion>

        <Accordion
          expanded={expanded === 'blockstack'}
          onChange={handleAccordionChange('blockstack')}
        >
          <AccordionSummary><Typography variant="h1">blockstack</Typography></AccordionSummary>
          <AccordionDetails>
            <BlockstackSettings />
          </AccordionDetails>
        </Accordion>

        <Accordion
          expanded={expanded === 'corsproxies'}
          onChange={handleAccordionChange('corsproxies')}
        >
          <AccordionSummary><Typography variant="h1">cors proxies</Typography></AccordionSummary>
          <AccordionDetails>
            <CorsProxies />
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === 'keys'}
          onChange={handleAccordionChange('keys')}
        >
          <AccordionSummary>
            <Typography variant="h1">keys</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Keys />
          </AccordionDetails>
        </Accordion>
      </Box>
      </Container>
    </Fragment>
  );
};

export default Settings;
