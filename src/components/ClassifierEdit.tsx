import React, { useState, useEffect, useContext, useCallback, FunctionComponent } from 'react';
import { BlockstackStorageContext } from './BlockstackSessionProvider';
import { BlockstackSessionContext } from './BlockstackSessionProvider';
import { useClassifiers } from '../custom-hooks/useClassifiers';
import { ClassifierContext } from './ClassifierFetch';
import { CategoryContext } from './Categories';
import { TextField, Divider, Button } from '@mui/material';
// import QRCode from 'react-qr-code';
// import useQrcode from 'react-use-qrcode';
import { useDebouncedCallback } from 'use-debounce';

var fromEntries = require('fromentries');
var entries = require('object.entries');

const ClassifierEdit: FunctionComponent = () => {
  const blockstackStorageContext = useContext(BlockstackStorageContext);
  const blockstackStorage = Object.assign(blockstackStorageContext);

  const blockstackSessionContext = useContext(BlockstackSessionContext);
  const blockstackSession = Object.assign(blockstackSessionContext);

  const categoryContext = useContext(CategoryContext);
  const category = `${categoryContext}`;

  const classifierContext = useContext(ClassifierContext);
  const defaultJSON = JSON.stringify(Object.assign(classifierContext), null, 2);

  const { classifiers, setClassifiers } = useClassifiers({});

  const [classifierJSON, setClassifierJSON] = useState('{}');
  const bayes = require('classificator');

  const publishClassifier = useDebouncedCallback((classifierJSON: string) => {
    const contentToUpload = JSON.stringify(JSON.parse(`${classifierJSON}`), null, 2)
    //
    // entries(dispatchersForCategory)
    // .filter((dispatcherEntry: [string, {keys: string[]}]) => [dispatcherEntry[1].keys].flat() !== [])
    // .forEach((dispatcherEntry: [string, any])   => {
    //   const dispatcherEntryObj = [...dispatcherEntry].slice()
    //   const recipientPublicKeys = entries(keys as object)
    //     .filter((key: [string, string]) => {
    //       return [[dispatcherEntryObj[1].keys].flat()
    //         .filter((dispatcherKey: any) => `${dispatcherKey}` === key[0])]
    //         .flat().length !== 0
    //     })
    //     .map((key: [string, {publicKey: string}]) => new Buffer(key[1].publicKey, 'base64'))
    //
    //   const classifierJSONEncrypted = box.multibox(
    //     new Buffer(`${contentToUpload}`),
    //     recipientPublicKeys, 10
    //   ).toString('base64')
    //
    //   // const secretkey = new Buffer('ENv0yOC4uQHDUoRw8aW7Efdb2B5j5YPl7vk75fwZeU8=', 'base64')
    //   // console.log(JSON.parse(box.multibox_open(new Buffer(classifierJSONEncrypted, 'base64'), secretkey).toString()))
    //   blockstackStorage.putFile(`share_${category}_${dispatcherEntryObj[0]}`, classifierJSONEncrypted, {
    //     encrypt: false
    //   })
    //   .then((successMessage: string) => {})
    //   .catch((error: any) => console.log(error))
    // })

    blockstackStorage.putFile(`classifiers_${category}`, contentToUpload, {
      encrypt: true
    })
    .then((successMessage: string) => {})
    .catch((error: object) => console.log(error))

  }, 10000, { leading: false })


  const setClassifierJSONCallback = useCallback(
    theClassifier => {
      const newClassifierJSON = `${theClassifier}`;
      setClassifierJSON(newClassifierJSON);
    },
    [setClassifierJSON]
  );

  useEffect(() => {
    // if (result) {
    //   alert(result)
    //   setClassifierJSONCallback(`${result}`);
    //
    // }
  }, [setClassifierJSONCallback]);

  const setClassifiersCallback = useCallback(
    newClassifiers => {
      const newClassifiersObj = Object.assign(newClassifiers as object);
      // console.log(newClassifiersObj)
      setClassifiers(newClassifiersObj);
    },
    [setClassifiers]
  );

  useEffect(() => {
    const fetchedClassifier = Object.assign(classifierContext as object);
    setClassifierJSONCallback(JSON.stringify(fetchedClassifier, null, 2));
  }, [classifierContext, setClassifierJSONCallback]);

  const onSubmit = (event: React.FormEvent) => {
    event.preventDefault();

    if (classifierJSON === undefined) {
      return;
    }
    try {
      const newClassifier = bayes.fromJson(classifierJSON);
      const newClassifierJSON = JSON.stringify(newClassifier);
      const newClassifiers: { [key: string]: object } = fromEntries([
        ...entries(classifiers as object).filter(
          (ObjectEntry: [string, unknown]) => ObjectEntry[0] !== category
        ),
        ...entries(classifiers as object)
          .filter((ObjectEntry: [string, unknown]) => ObjectEntry[0] === category)
          .map((ObjectEntry: [string, unknown]) => [ObjectEntry[0], newClassifier]),
      ]);

      setClassifiersCallback(newClassifiers);
      [blockstackSession.isUserSignedIn()]
        .filter(signedIn => !!signedIn)
        .forEach(() => {
          blockstackStorage
            .putFile(`classifiers_${category}`, newClassifierJSON, {
              encrypt: true,
            })
            .catch((e: object) => {
              // not logged into blockstack?
              console.log('error writing to blockstack');
            });
        })
    } catch (error) {
      const newClassifier = bayes();
      const newClassifierJSON = JSON.stringify(newClassifier);
      const newClassifiers: { [key: string]: object } = fromEntries([
        ...entries(classifiers as object).filter(
          (ObjectEntry: [string, unknown]) => ObjectEntry[0] !== category
        ),
        ...entries(classifiers as object)
          .filter((ObjectEntry: [string, unknown]) => ObjectEntry[0] === category)
          .map((ObjectEntry: [string, unknown]) => [ObjectEntry[0], newClassifier]),
      ]);
      setClassifiersCallback(newClassifiers);

      [blockstackSession.isUserSignedIn()]
        .filter(signedIn => !!signedIn)
        .forEach(() => {
          blockstackStorage
            .putFile(`classifiers_${category}`, newClassifierJSON, {
              encrypt: true,
            })
            .catch((e: object) => {
              // not logged into blockstack?
              console.log('error writing to blockstack');
            });
        })

      // {[blockstackSession.isUserSignedIn()]
      //   .filter(signedIn => !!signedIn)
      //   .forEach(() => {
      //     publishClassifier(newClassifierJSON);
      //   })}
      // blockstackStorage
      //   .putFile(`classifiers_${category}`, newClassifierJSON, {
      //     encrypt: true,
      //   })
      //   .catch((e: any) => {
      //     // not logged into blockstack?
      //     console.log('error writing to blockstack');
      //   });
    }
  };

  return (
    <>
      <form onSubmit={onSubmit}>
        <br />
        <TextField
          id={`edit-classifier-${category}`}
          label={`edit classifier: ${category}`}
          multiline
          maxRows={4}
          style={{ width: 500 }}
          defaultValue={defaultJSON}
          onChange={event => setClassifierJSONCallback(event.target.value)}
        />
        <Divider />
        <Button type="submit" variant="contained">
          submit
        </Button>
      </form>
      <br />


    </>
  );
};

export default ClassifierEdit;
