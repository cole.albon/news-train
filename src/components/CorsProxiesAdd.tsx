import React, { FunctionComponent, useState, useCallback } from 'react';
import { TextField } from '@material-ui/core';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';

const CorsProxiesAdd: FunctionComponent = () => {
  const { corsProxies } = useCorsProxies();
  const [inputValue, setInputValue] = useState('');
  const { setCorsProxies } = useCorsProxies();

  const setInputCallback = useCallback(
    (newInputValue: string) => {
      setInputValue(newInputValue);
    },
    [setInputValue]
  );

  const addCorsProxyCallback = useCallback(() => {
    const newCorsProxy = JSON.parse(`{"${inputValue}": {"checked": true}}`);
    setCorsProxies({ ...newCorsProxy, ...JSON.parse(JSON.stringify(corsProxies)) });
  }, [corsProxies, setCorsProxies, inputValue]);

  return (
    <>
      <TextField
        id="addCorsProxyTextField"
        placeholder="add corsproxy here"
        value={inputValue}
        onKeyPress={event => {
          [event.key]
            .filter(theKey => theKey === 'Enter')
            .map(() => {
              addCorsProxyCallback();
              setInputCallback('');
              return 'o';
            });
        }}
        onChange={event => {
          setInputCallback(event.target.value);
        }}
        fullWidth
      />
    </>
  );
};

export default CorsProxiesAdd;
