import React, { FunctionComponent } from 'react';

const PageHeader: FunctionComponent = ({ children }) => {
  return (
    <span style={{display:"flex", justifyContent:"center", fontSize:'150%'}}>
    {
      [children].flat().map((headingEntry, index) => {
        return (
          <h1 key={index}>{headingEntry}</h1>
        )
      })
    }
    </span>
  );
};

export default PageHeader;
