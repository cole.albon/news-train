import React, { useContext, FunctionComponent } from 'react';
import { BlockstackFilenamesContext } from './BlockstackFilenamesFetch';

const BlockstackFilenamesDisplay: FunctionComponent = () => {
  const blockstackFilenamesContext = useContext(BlockstackFilenamesContext);
  const blockstackFilenames = [...Object.values(blockstackFilenamesContext)]
  return <pre>{JSON.stringify(blockstackFilenames, null, 2)}</pre>;
};

export default BlockstackFilenamesDisplay;
