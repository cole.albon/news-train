import React, { FunctionComponent } from 'react';
import Button from '@material-ui/core/Button';
import { useFeeds } from '../custom-hooks/useFeeds';

const FeedsReset: FunctionComponent = () => {
  const { factoryReset } = useFeeds();
  return (
    <Button key="feedsreset" onClick={() => factoryReset()}>
      reset feeds
    </Button>
  );
};
export default FeedsReset;
