import React, { FunctionComponent, useContext, createContext, Fragment } from 'react';
import 'react-block-ui/style.css';

import { RouteComponentProps } from '@reach/router';
import {
  Box,
  Link,
  List,
  ListSubheader,
  Typography,
} from '@material-ui/core';
import htmlToText from 'html-to-text';
import { useFeeds } from '../custom-hooks/useFeeds';
import { useCategories } from '../custom-hooks/useCategories';
import { CategoryContext } from './Categories';
import { ParsedFeedContentContext } from './FeedContentParse';
import ProcessedPostsFetch from './ProcessedPostsFetch';
import ClassifierFetch from './ClassifierFetch';
import DispatchersFetch from './DispatchersFetch';
import BayesPredictionDisplay from './BayesPredictionDisplay';
import ApplyBayesPrediction from './ApplyBayesPrediction';
import SurpressNotGoodAboveThresholdPosts from './SurpressNotGoodAboveThresholdPosts';
import Post from './Post';
import SurpressProcessedPosts from './SurpressProcessedPosts';
import MarkFeedProcessedButton from './MarkFeedProcessedButton'
import { removePunctuation } from '../index.js';
import BlockstackFilenamesFetch from './BlockstackFilenamesFetch';

export const PostContext = createContext({});
export const FeedContext = createContext({});

const Posts: FunctionComponent<RouteComponentProps> = (props: RouteComponentProps) => {
  const { categories } = useCategories();
  const { feeds } = useFeeds();
  const parsedFeedContentContext = useContext(ParsedFeedContentContext);
  const parsedFeedContent = Object.assign(parsedFeedContentContext);

  const checkedCategories = Object.entries(JSON.parse(JSON.stringify(categories)))
    .filter(category => Object.assign(category[1] as object).checked === true)
    .map(category => category[0]);

  const feedsForCategory = (category: string) => {
    const theFeeds = Object.entries(JSON.parse(JSON.stringify(feeds)))
      .filter(feedEntry => {
        return Object.entries(JSON.parse(JSON.stringify(feedEntry[1])))
          .filter(feedEntryAttribute => {
            return feedEntryAttribute[0] === 'categories';
          })
          .find(feedEntryAttribute => {
            return [feedEntryAttribute[1]].flat().indexOf(category) !== -1;
          });
      })
      .filter(feedEntry => {
        return Object.entries(JSON.parse(JSON.stringify(feedEntry[1])))
          .filter(feedEntryAttribute => {
            return feedEntryAttribute[0] === 'checked';
          })
          .filter(feedEntryAttribute => {
            return feedEntryAttribute[1] === true;
          })
          .find(() => true);
      });
    return theFeeds;
  };

  return (
    <BlockstackFilenamesFetch>
      <Box style={{ maxWidth: 600 }}>
        <List subheader={<li />}>
          {checkedCategories.map(category => {
            const checkedCategory = `${category}`;
            return (
              <Fragment key={category}>
                <CategoryContext.Provider key={checkedCategory} value={checkedCategory}>
                  <ClassifierFetch>
                    <DispatchersFetch>
                      <ListSubheader>
                        <Box style={{ backgroundColor: 'white', minWidth: '600' }}> </Box>
                      </ListSubheader>
                      <ListSubheader>
                        <Box
                          style={{
                            backgroundColor: 'white',
                            minWidth: '600',
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            maxWidth: 600,
                          }}
                        >
                          <Typography variant="h1">{`${checkedCategory}`}</Typography>
                        </Box>
                      </ListSubheader>
                      <li key={`section-${checkedCategory}`}>
                        <ul>
                          {feedsForCategory(checkedCategory).map(feed => {
                            const feedItem = Object.assign(feed);
                            const shortFeedTitle = feedItem[0]
                              .replace(/(^\w+:|^)\/\//, '')
                              .slice(0, 16);
                            return (
                              <FeedContext.Provider key={JSON.stringify(feedItem)} value={feedItem}>
                                <ProcessedPostsFetch>
                                  <SurpressProcessedPosts>
                                    <ListSubheader>
                                      <Box
                                        style={{
                                          textAlign: 'center',
                                          backgroundColor: 'rgba(0,0,0,0)',
                                          minWidth: '100%',
                                          flexDirection: 'row',
                                          height: '100px',
                                          fontSize: 60,
                                          justifyContent: 'center',
                                        }}
                                      >
                                        <MarkFeedProcessedButton />
                                        <Link href={feedItem[0]}>{shortFeedTitle}</Link>
                                      </Box>

                                    </ListSubheader>
                                    <Box style={{ maxWidth: 600 }}>
                                      <Box
                                        style={{
                                          justifyContent: 'center',
                                          flexDirection: 'column',
                                        }}
                                      >
                                        {Object.entries(parsedFeedContent as object)
                                          .filter(feedContentEntry => {
                                            return feedContentEntry[0] === feed[0];
                                          })
                                          .filter(parsedFeedEntry => {
                                            return parsedFeedEntry[1];
                                          })
                                          .map(parsedFeedEntry => {
                                            return parsedFeedEntry[1][0];
                                          })
                                          .map(parsedFeedEntryContent => {
                                            return parsedFeedEntryContent
                                              .filter(
                                                (
                                                  postItem: {
                                                    title?: string;
                                                    link?: string;
                                                    description?: string;
                                                  },
                                                  idx: number
                                                ) => {
                                                  return !!postItem.link;
                                                }
                                              )
                                              .filter(
                                                (
                                                  postItem: {
                                                    title?: string;
                                                    link: string;
                                                    description?: string;
                                                  },
                                                  idx: number
                                                ) => {
                                                  return !!postItem.title;
                                                }
                                              )
                                              .map(
                                                (
                                                  postItem: {
                                                    title: string;
                                                    link?: string;
                                                    description?: string;
                                                  },
                                                  idx: number
                                                ) => {
                                                  const theDescription = `${postItem.description}`;
                                                  const descriptionText = htmlToText.fromString(
                                                    theDescription,
                                                    {
                                                      ignoreHref: true,
                                                      ignoreImage: true,
                                                    }
                                                  );
                                                  const theTitle = `${postItem.title}`;
                                                  const mlText = removePunctuation(
                                                    `${theTitle} ${descriptionText}`
                                                  );
                                                  return (
                                                    <Fragment key={idx}>
                                                      <Box style={{ textAlign: 'center' }}>
                                                        <Box style={{ maxWidth: 600 }}>
                                                          <Fragment key={mlText.replace(/ /g, '-')}>
                                                            <PostContext.Provider
                                                              value={postItem}
                                                              key={JSON.stringify(postItem)}
                                                            >
                                                              <SurpressProcessedPosts>
                                                                <ApplyBayesPrediction>
                                                                  <SurpressNotGoodAboveThresholdPosts>
                                                                    <Box
                                                                      style={{
                                                                        textAlign: 'center',
                                                                      }}
                                                                    >
                                                                      <Box
                                                                        style={{ maxWidth: 600 }}
                                                                      >
                                                                        <Post />
                                                                        <BayesPredictionDisplay />
                                                                      </Box>
                                                                    </Box>
                                                                  </SurpressNotGoodAboveThresholdPosts>
                                                                </ApplyBayesPrediction>
                                                              </SurpressProcessedPosts>
                                                            </PostContext.Provider>
                                                          </Fragment>
                                                        </Box>
                                                      </Box>
                                                    </Fragment>
                                                  );
                                                }
                                              );
                                          })}
                                      </Box>
                                    </Box>
                                  </SurpressProcessedPosts>
                                </ProcessedPostsFetch>
                              </FeedContext.Provider>
                            );
                          })}
                        </ul>
                      </li>
                    </DispatchersFetch>
                  </ClassifierFetch>
                </CategoryContext.Provider>
              </Fragment>
            );
          })}
        </List>
      </Box>
    </BlockstackFilenamesFetch>
  );
};

export default Posts;
