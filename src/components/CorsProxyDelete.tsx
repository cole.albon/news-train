import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { IconButton } from '@material-ui/core';
import DeleteOutlined from '@material-ui/icons/DeleteOutlined';
import { useCorsProxies } from '../custom-hooks/useCorsProxies';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const CorsProxyDelete: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { corsProxies, setCorsProxies } = useCorsProxies();

  const deleteCorsProxy = useCallback(() => {
    const newCorsProxies = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(corsProxies))).filter(
            (corsProxy: [string, unknown]) => corsProxy[0] !== props.text
          )
        ),
      })
    );
    setCorsProxies(newCorsProxies);
  }, [corsProxies, props.text, setCorsProxies]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(corsProxies)))
        .filter((corsProxy: [string, unknown]) => corsProxy[0] === props.text)
        .map((corsProxy: [string, unknown]) => {
          return (
            <Fragment key={`${corsProxy}`}>
              <IconButton
                aria-label="Delete CorsProxy"
                onClick={deleteCorsProxy}
              >
                <DeleteOutlined />
              </IconButton>
            </Fragment>
          );
        })}
    </Fragment>
  );
};

export default CorsProxyDelete;
