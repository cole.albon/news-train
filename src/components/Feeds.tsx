import React, { FunctionComponent } from 'react';
import { RouteComponentProps } from '@reach/router';
import { Box, Divider } from '@material-ui/core';
import FeedsAdd from './FeedsAdd';
import FeedsJSON from './FeedsJSON';
import FeedsReset from './FeedsReset';
import FeedToggle from './FeedToggle';
import FeedDelete from './FeedDelete';
import FeedCategories from './FeedCategories';
import { useFeeds } from '../custom-hooks/useFeeds';

const Feeds: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const { feeds } = useFeeds();
  return (
    <>
      <Box p={1}>
        <FeedsAdd />
      </Box>
      <Box p={1} maxWidth={600}>
        {Object.keys(JSON.parse(JSON.stringify(feeds))).map(feed => {
          return (
            <Box key={feed}>
              <Box display="flex">
                <Box width="100%">
                  <FeedToggle text={feed} />
                </Box>
                <Box flexShrink={0}>
                  <FeedDelete text={feed} />
                </Box>
              </Box>
              <Box>
                <FeedCategories text={feed} />
              </Box>
            </Box>
          );
        })}
      </Box>
      <Box p={1}>
        <FeedsReset />
      </Box>
      <Box display="none">
        <Divider />
        <FeedsJSON />
      </Box>
      <Box display="none">
        <Divider />
        <pre>{JSON.stringify(Object.keys(JSON.parse(JSON.stringify(feeds))), null, 2)}</pre>
      </Box>
    </>
  );
};

export default Feeds;
