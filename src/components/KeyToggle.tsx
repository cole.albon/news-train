import React, { useCallback, FunctionComponent, Fragment } from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';
import { useKeys } from '../custom-hooks/useKeys';
var fromEntries = require('fromentries')
var entries = require('object.entries')

const KeyToggle: FunctionComponent<{ text: string }> = (props: {
  text: string;
}) => {
  const { keys, setKeys } = useKeys();

  const setKeysCallback = useCallback(() => {
    const newKey = JSON.parse(
      JSON.stringify({
        ...fromEntries(
          entries(JSON.parse(JSON.stringify(keys)))
            .filter((key: [string, unknown]) => key[0] === props.text)
            .map((key: [string, unknown]) => {
              return [
                key[0],
                {
                  ...fromEntries(
                    entries({
                      ...(key[1] as Record<string, unknown>),
                    })
                      .filter(
                        (attribute: [string, unknown]) =>
                          attribute[0] === 'checked'
                      )
                      .map((attribute: [string, unknown]) => [attribute[0], !attribute[1]])
                  ),
                  ...fromEntries(
                    entries({
                      ...(key[1] as Record<string, unknown>),
                    }).filter((attribute: [string, unknown]) => attribute[0] !== 'checked')
                  ),
                },
              ];
            })
        )
      })
    );
    setKeys({ ...JSON.parse(JSON.stringify(keys)), ...newKey });
  }, [keys, props.text, setKeys]);

  return (
    <Fragment>
      {entries(JSON.parse(JSON.stringify(keys)))
        .filter((key: [string, unknown]) => key[0] === props.text)
        .map((key: [string, unknown]) => {
          const attributes = key[1] as Record<string, unknown>;
          return (
            <FormControlLabel
              key={key[0]}
              control={
                <Switch
                  checked={Object.values(
                    fromEntries(
                      entries(attributes).filter(
                        (attribute: [string, unknown]) => attribute[0] === 'checked'
                      )
                    )
                  ).some(checked => checked)}
                  onChange={() => setKeysCallback()}
                  name={props.text}
                />
              }
              label={props.text}
            />
          );
        })}
    </Fragment>
  );
};

export default KeyToggle;
