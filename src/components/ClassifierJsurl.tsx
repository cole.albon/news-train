import React, {
  useContext,
  FunctionComponent,
  Fragment
} from 'react';
import { ClassifierContext } from './ClassifierFetch';
import { CategoryContext } from './Categories';

var JSURL = require("jsurl");

const ClassifierJsurl: FunctionComponent = () => {
  const classifierContext = useContext(ClassifierContext);
  const classifier = Object.assign(classifierContext);
  const categoryContext = useContext(CategoryContext);
  const category = `${categoryContext}`
  const str = btoa(JSURL.stringify(JSON.parse(`{classifier: {${category}: ${JSON.stringify(classifier)}}}`)))
  return (
    <Fragment>
      share:
      <a href={`news?classifieroverride=${str}`}>{`${category} classifier`}</a>
    </Fragment>
  )
};

export default ClassifierJsurl;
