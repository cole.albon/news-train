import React, { FunctionComponent,Fragment } from 'react';
import { Box, Link, Divider } from '@material-ui/core';
import { RouteComponentProps } from '@reach/router';
import NavBar from './NavBar'
import { CssBaseline, Container } from '@mui/material';

const Payment: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  return (
    <Fragment>
      <CssBaseline />
      <Container maxWidth="sm">
        <Box sx={{display: 'flex', flexDirection: 'column'}}>
          <NavBar>
            <Link href="/news">news</Link>
            <Link href="/settings">settings</Link>
            <Link href="/classifiers">classifiers</Link>
          </NavBar>
          <Divider />
          <Box sx={{display: 'flex', flexDirection: 'column'}}>
            Payment
          </Box>
        </Box>
      </Container>
    </Fragment>
  );
};

export default Payment;
