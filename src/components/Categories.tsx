import React, { FunctionComponent, createContext } from 'react';
import { Box, Divider } from '@material-ui/core';
import CategoriesAdd from './CategoriesAdd';
import CategoriesJSON from './CategoriesJSON';
import CategoriesReset from './CategoriesReset';
import CategoryToggle from './CategoryToggle';
import CategoryDelete from './CategoryDelete';
import { RouteComponentProps } from '@reach/router';
import { useCategories } from '../custom-hooks/useCategories';

export const CategoryContext = createContext('');

const Categories: FunctionComponent<RouteComponentProps> = (
  props: RouteComponentProps
) => {
  const { categories } = useCategories();
  return (
    <>
      <Box p={1}>
        <CategoriesAdd />
      </Box>
      <Box p={1} maxWidth={600}>
        {Object.keys(JSON.parse(JSON.stringify(categories))).map(category => {
          return (
            <Box display="flex" key={category}>
              <Box width="100%">
                <CategoryToggle text={category} />
              </Box>
              <Box flexShrink={0}>
                <CategoryDelete text={category} />
              </Box>
            </Box>
          );
        })}
      </Box>

      <Box p={1}>
        <CategoriesReset />
      </Box>
      <Box display="none">
        <Divider />
        <CategoriesJSON />
      </Box>
      <Box display="none">
        <Divider />
        <pre>{JSON.stringify(props, null, 2)}</pre>
      </Box>
    </>
  );
};

export default Categories;
