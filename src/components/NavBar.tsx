import React, { FunctionComponent, Fragment } from 'react';
import {Box, Typography, Paper} from '@material-ui/core';

const NavBar: FunctionComponent = ({ children }) => {
  return (
    <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={3}>
      <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center' }}>
      {
        [children].flat().map((headingEntry, index) => {
          return (
            <Fragment key={index}>
            <Typography variant="h4" gutterBottom>
            <Box sx={{ margin: 2 }} key={index}>{headingEntry}</Box>
            </Typography>
            </Fragment>
          )
        })
      }
      </Box>
    </Paper>
  );
};

export default NavBar;
